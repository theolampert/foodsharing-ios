//
//  ChatViewController.swift
//  foodsharing-ios
//
//  Created by Baudisgroup User on 08.04.19.
//  Copyright © 2019 com.kaleidosstudio. All rights reserved.
//

import UIKit
import MessageKit
import MessageInputBar
import SocketIO

class ChatViewController: MessagesViewController {

    var test: String?
    var conversation: Conversation?
    var messages: [Message] = [] {
        didSet {
            DispatchQueue.main.async {

                self.messagesCollectionView.reloadData()
                self.messagesCollectionView.scrollToBottom(animated: false)
            }
        }
    }
    var myself = Member(name: UserDefaults.standard.string(forKey: "name")!, id: UserDefaults.standard.integer(forKey: "id"))
    var members: [Member] = []


    // MARK: - Properties

    override var inputAccessoryView: UIView? {
        return customMessageInputBar
    }
    override var canBecomeFirstResponder: Bool {
        return true
    }
    // MARK: - MessageInputBar
    private let customMessageInputBar = CustomInputBar()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.conversation!.sender!

        NotificationCenter.default.addObserver(self, selector: #selector(self.handleConvNotification(_:)), name: .gotConvFromSocket, object: nil)


        LoadMessages(conversation_id: self.conversation!.id!)

        SocketIOManager.sharedInstance.establishConnection()

        // MARK: - MessageInputBar



        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        customMessageInputBar.delegate = self
        messagesCollectionView.messagesDisplayDelegate = self


        let backButton = UIBarButtonItem (image: UIImage(named: "arrow_left"), style: .plain, target: self, action: #selector(goBackReloadConversation))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.hidesBackButton = true

    }

    @objc func goBackReloadConversation(){

        self.navigationController!.popViewController(animated: true)

        if let parent = self.parent as? navigationController{

            if let containerVC = parent.children[0] as? MessageViewController {
                containerVC.LoadConversations(afterRefresh: false)
            }

        }

    }


    // MARK: - Navigation

    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChatViewController {
    func LoadMessages (conversation_id: Int) {
        fetch_API_response(query_API: .getConversationByid, API_id: conversation_id, method: "GET", headers: nil, response_type: "dict", completion: {json, error in
            if let json = json as! [String:Any]? {
//                print(json)

                var tmp_img: String

                for mem in json["members"] as! [[String:Any]] {

                    if let image_url = mem["avatar"] as? String {

                        tmp_img = Constants.base_url + "/images/130_q_" + image_url

                    } else {

                        tmp_img = Constants.DEFAULT_USER_PICTURE

                    }

                    let tmp_mem = Member(name: mem["name"]! as! String, id: mem["id"]! as! Int)
                    tmp_mem.img = tmp_img
                    tmp_mem.downloadImage(from: URL(string: tmp_img)!)
                    self.members.append(tmp_mem)

                }

                var tmp_messages: [Message] = []
                for msg in json["messages"] as! [[String:Any]]{


                    let tmp_member = self.members.filter {
                                $0.id == msg["fs_id"]! as! Int
                                }

                    var msg_sender = Member(name: msg["fs_name"]! as! String, id: msg["fs_id"]! as! Int)

                    if tmp_member.count == 0 {
                        if let image_url = msg["fs_photo"] as? String {
                            msg_sender.img = Constants.base_url + "/images/130_q_" + image_url
                        } else {
                            msg_sender.img = Constants.DEFAULT_USER_PICTURE
                        }
                        msg_sender.downloadImage(from: URL(string: msg_sender.img!)!)
                        self.members.append(msg_sender)
                    } else {
                        msg_sender = tmp_member[0]
                    }

                    var tmp_body = msg["body"]! as! String
                    tmp_body = (tmp_body.htmlAttributedString?.string)!
                    let tmp_msg = Message(member: msg_sender, text: tmp_body, messageId: String(msg["id"]! as! Int), time: msg["time"] as! String)

                    tmp_messages.append(tmp_msg)
//                    self.messages = tmp_messages

                }
                 self.messages = tmp_messages.sorted(by: {$0.time_ts < $1.time_ts})
            }

        })
    }
}
extension ChatViewController: MessagesDataSource {
    func numberOfSections(
        in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }

    func currentSender() -> Sender {
        return Sender(id: myself.name, displayName: myself.name)
    }

    func messageForItem(
        at indexPath: IndexPath,
        in messagesCollectionView: MessagesCollectionView) -> MessageType {

        return messages[indexPath.section]
    }

    func messageTopLabelHeight(
        for message: MessageType,
        at indexPath: IndexPath,
        in messagesCollectionView: MessagesCollectionView) -> CGFloat {

        return 16
    }

    func messageTopLabelAttributedText(
        for message: MessageType,
        at indexPath: IndexPath) -> NSAttributedString? {

        return NSAttributedString(
            string: message.sender.displayName,
            attributes: [.font: UIFont.systemFont(ofSize: 13)])
    }

    func messageBottomLabelHeight(
        for message: MessageType,
        at indexPath: IndexPath,
        in messagesCollectionView: MessagesCollectionView) -> CGFloat {

        return 12
    }

    func messageBottomLabelAttributedText(
        for message: MessageType,
        at indexPath: IndexPath) -> NSAttributedString? {

        return NSAttributedString(
            string: MessageKitDateFormatter_German.shared.string(from: message.sentDate),
            attributes: [.font: UIFont.systemFont(ofSize: 10), .foregroundColor: UIColor.gray])
    }


}

extension ChatViewController: MessagesLayoutDelegate {
    func heightForLocation(message: MessageType,
                           at indexPath: IndexPath,
                           with maxWidth: CGFloat,
                           in messagesCollectionView: MessagesCollectionView) -> CGFloat {

        return 0
    }

    func cellTopLabelAlignment(for message: MessageType,
                               at indexPath: IndexPath,
                               in messagesCollectionView: MessagesCollectionView) -> LabelAlignment {
        return LabelAlignment(textAlignment: .center, textInsets: UIEdgeInsets(top: 0, left: 0, bottom: 3, right: 0))
    }

    func cellBottomLabelAlignment(for message: MessageType,
                                  at indexPath: IndexPath,
                                  in messagesCollectionView: MessagesCollectionView) -> LabelAlignment {
        return LabelAlignment(textAlignment: .center, textInsets: UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0))
    }

    func avatarSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        return CGSize(width: 40, height: 40)
    }
}

extension ChatViewController: MessagesDisplayDelegate {
    func configureAvatarView(
        _ avatarView: AvatarView,
        for message: MessageType,
        at indexPath: IndexPath,
        in messagesCollectionView: MessagesCollectionView) {

        let message = messages[indexPath.section]
        avatarView.image = message.member.image
    }

    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(corner, .pointedEdge)
    }

    func backgroundColor(for message: MessageType, at indexPath: IndexPath,
                         in messagesCollectionView: MessagesCollectionView) -> UIColor {

        return isFromCurrentSender(message: message) ? custom_color.leaf : custom_color.incomingGray

    }


}

extension ChatViewController: MessageInputBarDelegate {
    internal func messageInputBar(
        _ inputBar: MessageInputBar,
        didPressSendButtonWith text: String) {

//        // UI change, maybe just wait to get the message from server
//        let newMessage = Message(
//            member: myself,
//            text: text,
//            messageId: UUID().uuidString,
//            time: DateFormatter().message_dateFormatter.string(from: Date()))
//
//        messages.append(newMessage)
        inputBar.inputTextView.text = ""
//
//        messagesCollectionView.reloadData()
//        messagesCollectionView.scrollToBottom(animated: true)

        // post message
        let request_header = [["c":String(self.conversation!.id!), "b": text]]
        post_API(query_API: .postMessage, API_id: 0, method: "POST", headers: request_header)
    }

}


extension ChatViewController {
    @objc func handleConvNotification(_ notification: Notification) {

        if let data = notification.object as? [[String: String]]{

            let message_content = data[0]["o"]

            var json:Any?

            do
            {
                json = try JSONSerialization.jsonObject(with: (message_content!.data(using: .utf8)!), options: [])

                if let json = json as? [String:Any] {

                    let name = json["fs_name"] as! String
                    let id = json["fs_id"] as! Int
                    let text = json["body"] as! String
                    let messageId = String(json["id"] as! Int)
                    let cid = json["cid"] as! Int
                    let time = json["time"] as! String


                    if cid == self.conversation!.id! {

                        let newMessage = Message(
                            member: Member(name: name, id: id),
                            text: text,
                            messageId: messageId,
                            time: time)

//                        print(newMessage)
                        messages.append(newMessage)

                    }
                }
            }
            catch
            {
                return
            }
        }







    }
}
