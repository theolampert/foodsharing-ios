//
//  Message.swift
//  LoginScreenSwift
//
//  Created by Baudisgroup User on 07.04.19.
//  Copyright © 2019 com.kaleidosstudio. All rights reserved.
//

import Foundation
import UIKit
import MessageKit

class Conversation {

    // MARK: - Properties
    var id: Int?
    var sender: String?
    var time: String?
    var line: NSAttributedString?
    var image_url: String?
    
}



class Member {
    let name: String
    let id: Int
    var img: String? {
        didSet {
            
        }
    }
    var image: UIImage?
//    var image = UIImage(named: "mom")
    
    init(name: String, id: Int) {
        self.name = name
        self.id = id
    }
}

extension Member {
    func downloadImage(from url: URL) {
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            guard let data = data, error == nil else {return}

            self.image = UIImage(data: data)

        })
        task.resume()
//        print("this is when I download the image!")
//        print(self.image)
    }
}

struct Message {
    let member: Member
    let text: String
    let messageId: String
    let time: String
    var time_ts: Double {
        get {
            return((DateFormatter().message_dateFormatter.date(from: time)?.timeIntervalSince1970)!)
        }
    }
}

extension Message: MessageType {
    var sender: Sender {
        return Sender(id: member.name, displayName: member.name)
    }
    
    var sentDate: Date {
        return((DateFormatter().message_dateFormatter.date(from: self.time))!)
    }
    
    var kind: MessageKind {
        let message = text
        let font = UIFont.systemFont(ofSize: 15)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        var color: UIColor
        if self.member.id == UserDefaults.standard.integer(forKey: "id") {
            color = .white
        } else {
            color = .darkText
        }
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .paragraphStyle: paragraphStyle,
            .foregroundColor: color
        ]
        let attributed = NSAttributedString.init(string: message, attributes: attributes)
        return .attributedText(attributed)
    }
}

