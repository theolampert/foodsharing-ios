//
//  errorHandeler.swift
//  foodsharing-ios
//
//  Created by Baudisgroup User on 09.04.19.
//  Copyright © 2019 com.kaleidosstudio. All rights reserved.
//

import Foundation

enum loginError: Error {
    case wrongPassword
    case noConnection
}


extension Notification.Name {
    static let login_success = Notification.Name("login_success")
    static let wrongPassword = Notification.Name("wrongPassword")
    static let noConnection = Notification.Name("noConnection")
    static let gotConvFromSocket = Notification.Name("gotConvFromSocket")
}
